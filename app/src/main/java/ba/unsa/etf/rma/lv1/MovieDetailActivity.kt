package ba.unsa.etf.rma.lv1

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import ba.unsa.etf.rma.lv1.data.Movie
import ba.unsa.etf.rma.lv1.viewmodel.MovieDetailViewModel

class MovieDetailActivity : AppCompatActivity() {
    private var movieDetailViewModel = MovieDetailViewModel();
    private lateinit var movie: Movie;
    private lateinit var title: TextView;
    private lateinit var overview: TextView;
    private lateinit var releaseDate: TextView;
    private lateinit var genre: TextView;
    private lateinit var website: TextView;
    private lateinit var poster: ImageView;
    private lateinit var addFavorite : Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        title = findViewById(R.id.movie_title);
        overview = findViewById(R.id.movie_overview);
        releaseDate = findViewById(R.id.movie_release_date);
        genre = findViewById(R.id.movie_genre);
        poster = findViewById(R.id.movie_poster);
        website = findViewById(R.id.movie_website);
        addFavorite = findViewById(R.id.addFavouriteBtn);

        val extras = intent.extras;

        if (extras != null) {
            movie = movieDetailViewModel.getMovieByTitle(extras.
                getString("movie_title",""));

            populateDetails();
        }
        else {
            finish();
        }
        // Website listener:
        website.setOnClickListener{
            showWebsite();
        }
        addFavorite.setOnClickListener { writeDB() }
    }

    private fun populateDetails() {
        title.text = movie.title;
        Log.v("TEST", movie.title);
        Log.v("TEST", title.text.toString());
        releaseDate.text = movie.releaseDate;
        //genre.text = movie.genre;
        website.text = movie.homepage;
        overview.text = movie.overview;

        val context: Context = poster.context;
        //var id: Int = context.resources.getIdentifier(movie.genre,
        //    "drawable", context.packageName);

        //if (id == 0) id = context.resources.getIdentifier("defaultpic",
        //        "drawable", context.packageName);

        var id : Int = context.resources.getIdentifier("defaultpic", "drawable",
        context.packageName);
        poster.setImageResource(id);
    }

    private fun showWebsite() {
        val webIntent: Intent = Uri.parse(movie.homepage).let {
            webpage -> Intent(Intent.ACTION_VIEW, webpage);
        }

        try {
            startActivity(webIntent);
        } catch (e: ActivityNotFoundException) {
            Log.v("EXCEPTION", "Nije pronadjena aplikacija");
        }
    }

    fun writeDB(){
        movieDetailViewModel.writeDB(applicationContext,this.movie,onSuccess =
        ::onSuccess1,
            onError = ::onError)
    }

    fun onSuccess1(message:String){
        val toast = Toast.makeText(applicationContext, "Spaseno", Toast.LENGTH_SHORT)
        toast.show()
        addFavorite.visibility= View.GONE
    }
    fun onError() {
        val toast = Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT)
        toast.show()
    }
}