package ba.unsa.etf.rma.lv1.data

fun favouriteMovies(): List<Movie> {
    return listOf(
        Movie(1, "The Shawshank Redemption",
        "Two imprisoned men bond over a number of years, finding solace and eventual " +
                "redemption through acts of common decency.",
        "1994", "https://www.imdb.com/title/tt0111161/?ref_=nv_sr_srsg_0",
        "drama","/q6y0Go1tsGEsmtFryDOJo3dEmqu.jpg"),

        Movie(2, "Pulp Fiction",
            "The lives of two mob hitmen, a boxer, a gangster and his wife, and a pair of " +
                    "diner bandits intertwine in four tales of violence and redemption.",
            "1994", "https://www.imdb.com/title/tt0110912/?ref_=nv_sr_srsg_0",
            "crime", "/fIE3lAGcZDV1G6XM5KmuWnNsPp1.jpg"),

        Movie(3, "Inglourious Basterds",
            "In Nazi-occupied France during World War II, a plan to assassinate Nazi leaders " +
                    "by a group of Jewish U.S. soldiers coincides with a theatre owner's " +
                    "vengeful plans for the same.",
            "2009", "https://www.imdb.com/title/tt0361748/?ref_=nv_sr_srsg_0",
            "war", "/7sfbEnaARXDDhKm0CZ7D7uc2sbo.jpg"),

        Movie(4, "21 Jump Street",
            "A pair of underachieving cops are sent back to a local high school to blend " +
                    "in and bring down a synthetic drug ring.",
            "2012", "https://www.imdb.com/title/tt1232829/?ref_=nv_sr_srsg_0",
            "comedy", "/8v3Sqv9UcIUC4ebmpKWROqPBINZ.jpg"),

        Movie(5, "Saw",
            "Two strangers awaken in a room with no recollection of how they got there, " +
                    "and soon discover they're pawns in a deadly game perpetrated by a notorious " +
                    "serial killer.",
            "2004", "https://www.imdb.com/title/tt0387564/?ref_=nv_sr_srsg_3",
            "horror", "/harQifr8kpIVqlLP41kTR058LZB.jpg"),

        Movie(6, "Who Am I",
            "Benjamin, a young German computer whiz, is invited to join a subversive hacker " +
                    "group that wants to be noticed on the world's stage.",
            "2014", "https://www.imdb.com/title/tt3042408/?ref_=nv_sr_srsg_0",
            "mystery","/nMHCJiVFjUmQWC116Ze96u5IhfX.jpg")
    );
}

fun recentMovies(): List<Movie> {
    return listOf(
        Movie(1, "The Batman",
            "When the Riddler, a sadistic serial killer, begins murdering key political " +
                    "figures in Gotham, Batman is forced to investigate the city's hidden corruption " +
                    "and question his family's involvement.",
            "2022", "https://www.imdb.com/title/tt1877830/?ref_=nv_sr_srsg_0",
            "action", "/74xTEgt7R36Fpooo50r9T25onhq.jpg"),

        Movie(2, "Dog",
            "Two former Army Rangers are paired against their will on the road trip of a " +
                    "lifetime. Briggs (Channing Tatum) and Lulu (a Belgian Malinois) race down " +
                    "the Pacific Coast to get to a fellow soldier's funeral on time.",
            "2022", "https://www.imdb.com/title/tt11252248/?ref_=nv_sr_srsg_3",
            "comedy", "/rkpLvPDe0ZE62buUS32exdNr7zD.jpg"),

        Movie(3, "Ambulance",
            "Two robbers steal an ambulance after their heist goes awry.",
            "2022", "https://www.imdb.com/title/tt4998632/?ref_=nv_sr_srsg_0",
            "action", "/zT5ynZ0UR6HFfWQSRf2uKtqCyWD.jpg"),

        Movie(4, "The Adam Project",
            "After accidentally crash-landing in 2022, time-traveling fighter pilot " +
                    "Adam Reed teams up with his 12-year-old self for a mission to save the future.",
            "2022", "https://www.imdb.com/title/tt2463208/?ref_=nv_sr_srsg_0",
            "action", "/wFjboE0aFZNbVOF05fzrka9Fqyx.jpg"),

        Movie(5, "Uncharted",
            "Street-smart Nathan Drake is recruited by seasoned treasure hunter Victor " +
                    "\"Sully\" Sullivan to recover a fortune amassed by Ferdinand Magellan, " +
                    "and lost 500 years ago by the House of Moncada.",
            "2022", "https://www.imdb.com/title/tt1464335/?ref_=nv_sr_srsg_0",
            "adventure", "/1hR8UOQ3NCmNM45XEj2WmjWfvul.jpg"),

        Movie(6, "Morbius",
            "Biochemist Michael Morbius tries to cure himself of a rare blood disease, " +
                    "but he inadvertently infects himself with a form of vampirism instead.",
            "2022", "https://www.imdb.com/title/tt5108870/?ref_=nv_sr_srsg_0",
            "action","/h4WLN3cmEjCsH1fNGRfvGV6IPBX.jpg")
    );
}