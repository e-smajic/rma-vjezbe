package ba.unsa.etf.rma.lv1.viewmodel

import android.content.Context
import ba.unsa.etf.rma.lv1.data.Movie
import ba.unsa.etf.rma.lv1.data.MovieRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class MovieDetailViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getMovieByTitle(name: String) : Movie {
        var movies: ArrayList<Movie> = arrayListOf();
        movies.addAll(MovieRepository.getRecentMovies());
        movies.addAll(MovieRepository.getFavouriteMovies());

        val movie = movies.find {movie -> name.equals(movie.title);};

        return movie?: Movie(0, "dummy", "dummy", "dummy", "dummy"
            , "dummy", "dummy");
    }

    fun writeDB(context: Context, movie:Movie, onSuccess: (movies: String) -> Unit,
                onError: () -> Unit) {
        scope.launch{
            val result = MovieRepository.writeFavorite(context,movie)
            when (result) {
                is String -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }
        }
    }
}