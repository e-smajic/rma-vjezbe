package ba.unsa.etf.rma.lv1

import ba.unsa.etf.rma.lv1.data.Movie
import ba.unsa.etf.rma.lv1.data.MovieRepository
import org.hamcrest.CoreMatchers.`is` as Is
import org.hamcrest.MatcherAssert.*
import org.hamcrest.Matchers.hasItem
import org.hamcrest.Matchers.not
import org.hamcrest.beans.HasPropertyWithValue.hasProperty
import org.junit.Assert.assertEquals
import org.junit.Test

class RepositoryUnitTest {
    @Test
    fun testGetFavoriteMovies(){
        val movies = MovieRepository.getFavouriteMovies();
        assertEquals(movies.size,6);
        assertThat(movies, hasItem<Movie>(hasProperty("title", Is("Pulp Fiction"))));
        assertThat(movies, not(hasItem<Movie>(hasProperty("title", Is("Forrest Gump")))));
    }

    @Test
    fun testGetRecentMovies(){
        val movies = MovieRepository.getRecentMovies();
        assertEquals(movies.size,6);
        assertThat(movies, hasItem<Movie>(hasProperty("title", Is("The Batman"))));
        assertThat(movies, not(hasItem<Movie>(hasProperty("title", Is("The Godfather")))));
    }
}